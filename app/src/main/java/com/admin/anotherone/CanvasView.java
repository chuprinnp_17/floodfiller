package ru.androiddevschool.floodfiller;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import com.flask.colorpicker.ColorPickerView;
import com.flask.colorpicker.OnColorSelectedListener;
import com.flask.colorpicker.builder.ColorPickerClickListener;
import com.flask.colorpicker.builder.ColorPickerDialogBuilder;
import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;

import org.adw.library.widgets.discreteseekbar.DiscreteSeekBar;

import java.util.ArrayList;

import nl.dionsegijn.steppertouch.StepperTouch;

public class CanvasView extends View {

    private int currentBackgroundColor = 0xff00ff00;
    private int currentWidth = 20;
    ColoringBitmap image;
    Context context;
    ArrayList<Integer> colors;

    private Rect dstRect;
    private Rect srcRect;
    private float ppuX;
    private float ppuY;
    private FloatingActionButton colorPickButton;
    private FloatingActionButton backColorButton;
    private FloatingActionMenu menuButton;
    private FloatingActionMenu mainMenu;
    private FloatingActionMenu backMenu;
    private Boolean eraserON = false;
    private int preEraserColor;



    public CanvasView(Context c, AttributeSet attrs) {
        super(c, attrs);
        context = c;
        image = new ColoringBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.i2));
        colors = new ArrayList<>();
        changeBackgroundColor(0xFFC0FA81);
     }

    // override onSizeChanged
    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
    }

    // override onDraw
    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (srcRect == null && dstRect == null) {
            srcRect = new Rect(0, 0, image.getCurrentBitmap().getWidth(), image.getCurrentBitmap().getHeight());
            dstRect = new Rect(0, 0, this.getWidth(), this.getHeight());
            ppuX = 1f * image.getCurrentBitmap().getWidth() / getWidth();
            ppuY = 1f * image.getCurrentBitmap().getHeight() / getHeight();
        }

        /*
        //TODO: поставить перерисовку прямых
        AsyncTask task = new AsyncTask() {
            @Override
            protected Object doInBackground(Object[] params) {
                return null;
            }
        };
        task.execute();*/
        image.step();

        canvas.drawBitmap(image.getCurrentBitmap(), srcRect, dstRect, null);

    }

    // when ACTION_DOWN start touch according to the x,y values
    private void startTouch(float x, float y) {
        image.start(x * ppuX, y * ppuY, currentBackgroundColor, currentWidth);
    }

    // when ACTION_MOVE move touch according to the x,y values
    private void moveTouch(float x, float y) {
        image.cont(x * ppuX, y * ppuY);
    }

    public void clearCanvas() {
        invalidate();
        image.clear();
        backMenu.close(true);
    }
    public void setWhiteColor(){
        invalidate();
        preEraserColor = currentBackgroundColor;
        currentBackgroundColor = 0xffffffff;
        eraserON = true;
        menuButton.close(true);
    }

    public void chooseColor() {
        invalidate();
        ColorPickerDialogBuilder
                .with(context)
                .setTitle("Choose color")
                .initialColor(currentBackgroundColor)
                .wheelType(ColorPickerView.WHEEL_TYPE.FLOWER)
                .density(12)
                .setOnColorSelectedListener(new OnColorSelectedListener() {
                    @Override
                    public void onColorSelected(int selectedColor) {
                        //toast("onColorSelected: 0x" + Integer.toHexString(selectedColor));
                    }
                })
                .setPositiveButton("ok", new ColorPickerClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int selectedColor, Integer[] allColors) {
                        changeBackgroundColor(selectedColor);
                        menuButton.close(true);
                    }
                })
                .setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                })
                .build()
                .show();
    }

    private void changeBackgroundColor(int selectedColor) {
        if (eraserON){
            if (backColorButton != null) backColorButton.setColorNormal(preEraserColor);
            this.currentBackgroundColor = selectedColor;
            // if (colorPickButton != null) colorPickButton.setColorNormal(selectedColor);
            if (menuButton != null) menuButton.setMenuButtonColorNormal(selectedColor);
            if (!colors.contains(selectedColor)) colors.add(selectedColor);
            eraserON = false;
        }
        //this.setBackgroundColor(selectedColor);

        if (backColorButton != null) backColorButton.setColorNormal(currentBackgroundColor);
        this.currentBackgroundColor = selectedColor;
        // if (colorPickButton != null) colorPickButton.setColorNormal(selectedColor);
        if (menuButton != null) menuButton.setMenuButtonColorNormal(selectedColor);
        if (!colors.contains(selectedColor)) colors.add(selectedColor);
    }


    // when ACTION_UP stop touch
    private void upTouch() {
        image.end();
    }

    //override the onTouchEvent
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        float x = event.getX();
        float y = event.getY();

        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                startTouch(x, y);
                invalidate();
                break;
            case MotionEvent.ACTION_MOVE:
                moveTouch(x, y);
                invalidate();
                break;
            case MotionEvent.ACTION_UP:
                upTouch();
                invalidate();
                break;
        }
        return true;
    }


    public void setColorPickButton(FloatingActionButton colorPickButton) {
        this.colorPickButton = colorPickButton;
    }

    public void setMenuButton(FloatingActionMenu menuButton) {
        this.menuButton = menuButton;
        this.menuButton.setMenuButtonColorNormal(currentBackgroundColor);
    }

    public void setBackColorButton(FloatingActionButton backColorButton) {
        this.backColorButton = backColorButton;
        this.backColorButton.setColorNormal(getPrevColor());
    }

    public void setMainMenuButton(FloatingActionMenu mainMenuButton) {
        this.mainMenu = mainMenuButton;
        mainMenuButton.setIconAnimated(false);
    }

    public void setBackMenuButton(FloatingActionMenu backMenuButton) {
        this.backMenu = backMenuButton;
        backMenuButton.setIconAnimated(false);
    }

    public void setprevColor() {
        changeBackgroundColor(getPrevColor());
    }

    public int getPrevColor() {
        if (eraserON) {
            int index = colors.indexOf(preEraserColor) - 1;
            if (index < 0) index += colors.size();
            return colors.get(index);
        }
        else {
            int index = colors.indexOf(currentBackgroundColor) - 1;
            if (index < 0) index += colors.size();
            return colors.get(index);
        }
    }

    public void discard() {
        image.prevResult();
        invalidate();
    }

    public void changeWidth(int width){
        currentWidth = width;
    }

    public void loadImage(Bitmap bitmap) {
        image = new ColoringBitmap(bitmap);
        mainMenu.close(true);
    }

    public void loadImage(String picturePath){
        if (picturePath!=null)
        {
            //System.out.println(picturePath);
            if (BitmapFactory.decodeFile(picturePath) == null) System.out.println("COULDN'T LOAD BITMAP");
            else image = new ColoringBitmap(BitmapFactory.decodeFile(picturePath));
        }
        mainMenu.close(true);
    }

    public Bitmap getBitmap() {
        return image.getCurrentBitmap();
    }

    public void incBrush() {
        currentWidth += 5;
        if (currentWidth > 50) currentWidth = 50;
    }

    public void decBrush() {
        currentWidth -= 5;
        if (currentWidth < 10) currentWidth = 10;
    }
}