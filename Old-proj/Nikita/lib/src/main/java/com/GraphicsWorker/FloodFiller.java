package com.GraphicsWorker;

import java.awt.Color;
import java.awt.Point;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Queue;

import javax.imageio.ImageIO;

/**
 * Created by Гриша on 19.02.2017.
 */
public class FloodFiller {
    public static void main(String[] args) throws IOException {
        String path = "D:/Desktop/Никита/";
        BufferedImage image = ImageIO.read(new File(path + "1.bmp"));
        int x = 290, y = 370;
        floodFill(image, x, y, Color.BLUE.getRGB());
        x = 100;
        y = 450;
        floodFill(image, x, y, Color.YELLOW.getRGB());
        x = 190;
        y = 350;
        pointFill(image, x, y, 100, Color.RED.getRGB());
        x = 290;
        y = 500;
        pointFill(image, x, y, 100, Color.GREEN.getRGB());
        x = 290;
        y = 500;
        pointFill(image, x, y, 100, Color.GREEN.getRGB());
        x = 360;
        y = 610;
        pointFill(image, x, y, 100, Color.CYAN.getRGB());
        x = 373;
        y = 462;
        floodFill(image, x, y, Color.MAGENTA.getRGB());

        ImageIO.write(image, "png", new File(path + "2.bmp"));
    }

    private static void pointFill(BufferedImage image, int x, int y, int radius, int color) {
        HashSet<Point> pointsForPainting = getFloodFillPoints(image, x, y);
        for (Point p : pointsForPainting) {
            if ((p.x - x) * (p.x - x) + (p.y - y) * (p.y - y) <= radius * radius) {
                image.setRGB(p.x, p.y, color);
            }
        }
    }

    private static void floodFill(BufferedImage image, int x, int y, int color) {
        HashSet<Point> points = getFloodFillPoints(image, x, y);
        for (Point p : points)
            image.setRGB(p.x, p.y, color);
    }

    private static HashSet<Point> getFloodFillPoints(BufferedImage image, int x, int y) {
        int sourceColor = image.getRGB(x, y);
        HashSet<Point> result = new HashSet<>();
        LinkedList<Point> queue = new LinkedList<>();
        Point p = new Point(x, y);
        queue.push(p);
        while (queue.size() > 0) {
            p = queue.poll();
            for (int i = Math.max(p.x - 1, 0); i <= Math.min(p.x + 1, image.getWidth() - 1); i++)
                for (int j = Math.max(p.y - 1, 0); j <= Math.min(p.y + 1, image.getHeight() - 1); j++)
                    if ((image.getRGB(i, j) == sourceColor) && (!result.contains(new Point(i, j)))) {
                        queue.add(new Point(i, j));
                        result.add(new Point(i, j));
                    }
        }
        return result;
    }
}
