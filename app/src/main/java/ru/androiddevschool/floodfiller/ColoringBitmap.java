package ru.androiddevschool.floodfiller;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.support.v4.util.Pools;

import java.util.ArrayList;
import java.util.Random;

public class ColoringBitmap {
    static Random r = new Random();
    private Bitmap image;
    private Bitmap originalImage;
    private Pools.Pool<ColouringPoint> pointsPool;
    private ArrayList<ColouringPoint> points;
    private ColouringPoint point;
    private int currentColor;
    private int currentRadius;
    private final int poolSize = 1000;
    private final int checkStep = 3;
    private ArrayList<Bitmap> stages;

    public ColoringBitmap(Bitmap bitmap) {
        bitmap = prepare(bitmap);
        image = bitmap.copy(Bitmap.Config.ARGB_4444, true);
        originalImage = bitmap.copy(Bitmap.Config.ARGB_4444, true);
        currentRadius = 20;
        pointsPool = new Pools.SimplePool<>(poolSize);
        for (int i = 0; i < poolSize; i++) pointsPool.release(new ColouringPoint());
        points = new ArrayList<>();
        stages = new ArrayList<>();
    }

    private Bitmap prepare(Bitmap bitmap) {
        Bitmap res = Bitmap.createScaledBitmap(bitmap, 540, 960, true);
        res = res.copy(Bitmap.Config.ARGB_4444, true);

        for (int i = 0; i < res.getWidth(); i++)
            for (int j = 0; j < res.getHeight(); j++)
                res.setPixel(i, j, (res.getPixel(i, j) < 0xff7f7f7f) ? 0xff000000 : 0xffffffff);
        return res;
    }

    public void start(float x, float y, int color, int radius) {
        saveState();
        currentColor = color;
        currentRadius = radius;
        addPoint(x, y);
    }

    public void cont(float x, float y) {
        if (!inImage(x, y)) return;
        if (inField(x, y)) {
            addPoint(x, y);
        } else {
            point.setDst(x, y);
        }
    }

    public void end() {
        for (ColouringPoint point : points) pointsPool.release(point);
        points.clear();
    }

    private void saveState() {
        stages.add(image.copy(Bitmap.Config.ARGB_4444, true));
    }

    private void addPoint(float x, float y) {
        if (point != null) point.setDst(x, y);
        point = pointsPool.acquire();
        if (point == null) return;
        point.set(x, y);
        point.fill();
        points.add(point);
    }

    public boolean inField(float x, float y) {
        for (ColouringPoint point : points)
            if (point.connected(x, y))
                return true;
        return false;
    }

    public boolean inImage(float x, float y) {
        return (x >= 0 && x < image.getWidth() && y >= 0 && y < image.getHeight());
    }

    public Bitmap getCurrentBitmap() {
        return image;
    }

    public void step() {
        for (ColouringPoint point : points) point.step();
    }

    public void clear() {
        image = originalImage.copy(Bitmap.Config.ARGB_4444, true);
    }

    public void prevResult() {
        if (!stages.isEmpty()){
            image.recycle();
            image = stages.remove(stages.size()-1);
        }
    }


    class ColouringPoint {
        Thread fillingThread;
        float x;
        float y;
        boolean moving;
        float dstx;
        float dsty;

        public ColouringPoint() {
            this(0, 0);
        }

        public ColouringPoint(float x, float y) {
            set(x, y);
        }

        private void set(float x, float y) {
            this.x = x;
            this.y = y;
            this.dstx = -1;
            this.dsty = -1;
            this.moving = false;
        }

        private void fill() {
            while (fillingThread != null && fillingThread.isAlive()) ;
            fillingThread = new Thread(new Runnable() {
                @Override
                public void run() {
                    float step = (float) (Math.PI / currentRadius / 3);
                    float tx, ty;
                    for (float phi = 0; phi <= 2 * Math.PI; phi += step) {
                        tx = x + (float) (currentRadius * Math.cos(phi));
                        ty = y + (float) (currentRadius * Math.sin(phi));
                        if (!line(tx, ty)) continue;
                    }
                }
            });
            fillingThread.start();
        }

        private boolean line(float x, float y) {
            float step = 1f / currentRadius;
            int clrX, clrY;
            for (float alpha = 0; alpha < 1.0f; alpha += step) {
                clrX = (int) ((1 - alpha) * this.x + alpha * x);
                clrY = (int) ((1 - alpha) * this.y + alpha * y);
                if (!inImage(clrX, clrY) || !canBeColoured(clrX, clrY)) return false;
                image.setPixel(clrX, clrY, currentColor);
            }
            return true;
        }

        public boolean connected(float x, float y) {
            float step = 2f / dist(x, y);
            int clrX, clrY;
            for (float alpha = 0; alpha < 1.0f; alpha += step) {
                clrX = (int) ((1 - alpha) * this.x + alpha * x);
                clrY = (int) ((1 - alpha) * this.y + alpha * y);
                if (!inImage(clrX, clrY)) continue;
                if (originalImage.getPixel(clrX, clrY) != Color.WHITE)
                    return false;
            }
            return true;
        }

        private boolean alreadyColoured(float x, float y) {
            return (image.getPixel((int) x, (int) y) == currentColor);
        }

        private boolean canBeColoured(float x, float y) {
            if (!inImage(x, y)) return false;
            return (originalImage.getPixel((int) x, (int) y) == Color.WHITE);
        }

        private float dist2(float x2, float y2) {
            return ((this.x - x2) * (this.x - x2) + (this.y - y2) * (this.y - y2));
        }

        private float dist(float x2, float y2) {
            return (float) Math.sqrt(dist2(x2, y2));
        }

        private void setDst(float x, float y) {
            if (dstx != -1) {
                moving = true;
                this.dstx = x;
                this.dsty = y;
            }
        }

        public void step() {
            if (!moving) return;
            float dst = dist(dstx, dsty);
            if (dst < currentRadius / 2) moving = false;
            if (moving) {
                x += (dstx - x) / dst * currentRadius;
                y += (dsty - y) / dst * currentRadius;
                fill();
            }
        }
    }
}