package ru.androiddevschool.floodfiller;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;

import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class PaintBox extends AppCompatActivity {
    private static final int REQUEST_CODE = 1;
    private static int RESULT_PHOTO = 1;
    private static int RESULT_LOAD_IMAGE = 2;
    private static String[] PERMISSIONS = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.CAMERA
    };
    private CanvasView customCanvas;
    private String mCurrentPhotoPath;
    private File mCurrentPhoto;

    ImageView ivPhoto;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_paint_box);
        customCanvas = (CanvasView) findViewById(R.id.signature_canvas);
        customCanvas.setColorPickButton((FloatingActionButton) findViewById(R.id.colorPicker));
        customCanvas.setBackColorButton((FloatingActionButton) findViewById(R.id.backColor));
        customCanvas.setMenuButton((FloatingActionMenu) findViewById(R.id.drawMenu));
        customCanvas.setMainMenuButton((FloatingActionMenu) findViewById(R.id.mainMenu));
        customCanvas.setBackMenuButton((FloatingActionMenu) findViewById(R.id.backMenu));

        verifyStoragePermissions(this);
        ivPhoto = (ImageView) findViewById(R.id.ivPhoto);


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK && requestCode == RESULT_LOAD_IMAGE) {
            customCanvas.loadImage(getPicturePath(data.getData()));
        }
        if (resultCode == RESULT_OK && requestCode == RESULT_PHOTO) {
            customCanvas.loadImage(getPic());
            galleryAddPic(mCurrentPhoto.getAbsolutePath());
            //saveBitmap(customCanvas.getBitmap());
        }
    }

    public static void verifyStoragePermissions(Activity activity) {
        int permission = ActivityCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE) | ActivityCompat.checkSelfPermission(activity, Manifest.permission.CAMERA);
        if (permission != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(
                    activity,
                    PERMISSIONS,
                    REQUEST_CODE
            );
        }
    }

    public void setEraser(View v) {
        customCanvas.setWhiteColor();
    }

    public void prevColor(View v) {
        customCanvas.setprevColor();
    }

    public void choseColor(View v) {
        customCanvas.chooseColor();
    }

    public void clearCanvas(View v) {
        customCanvas.clearCanvas();
    }

    public void incSize(View view) {
        customCanvas.incBrush();
    }

    public void decSize(View view) {
        customCanvas.decBrush();
    }

    public void sendImage(View v) {
        File file = saveBitmap(customCanvas.getBitmap());

        galleryAddPic(file.getAbsolutePath());

        Intent shareIntent = new Intent();
        shareIntent.setAction(Intent.ACTION_SEND);
        Uri uri = FileProvider.getUriForFile(this,
                "ru.androiddevschool.fileprovider",
                file);
        shareIntent.putExtra(Intent.EXTRA_STREAM, uri);
        shareIntent.putExtra(Intent.EXTRA_TEXT, "Братишка, я тебе картинку из PaintBox'а принес");
        shareIntent.setType("image/*");
        startActivity(Intent.createChooser(shareIntent, "Поделиться с другом"));
    }

    public void discard(View v) {
        customCanvas.discard();
    }

    public void loadImage(View vIew) {
        Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        i.setType("image/*");
        startActivityForResult(i, RESULT_LOAD_IMAGE);
    }

    public void onClickPhoto(View view) {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        if (intent.resolveActivity(getPackageManager()) != null) {
            // создаем файл где будет храниться фото
            File photoFile = null;
            try {
                photoFile = createImageFile();
                mCurrentPhoto = photoFile;
            } catch (IOException e) {
                e.printStackTrace();
            }
            //если файл был создан
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(this,
                        "ru.androiddevschool.fileprovider",
                        photoFile);
                intent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(intent, RESULT_PHOTO);
            }
        }
    }

    /* для подгрузки изображения из галереи */
    public String getPicturePath(Uri selectedImage) {
        String[] filePathColumn = {MediaStore.Images.Media.DATA};
        Cursor cursor = getContentResolver().query(selectedImage, filePathColumn, null, null, null);
        cursor.moveToFirst();
        int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
        String picturePath = cursor.getString(columnIndex);
        cursor.close();
        return picturePath;
    }

    private Bitmap getPic() {
        int targetW = 540;
        int targetH = 960;

        // Размеры оригинала
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);
        int photoW = bmOptions.outWidth;
        int photoH = bmOptions.outHeight;

        // Коэффициенты пропорциональности
        int scaleFactor = Math.min(photoW / targetW, photoH / targetH);

        // Получаем битмап
        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = scaleFactor;
        bmOptions.inMutable = true;

        return BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);
    }

    private void galleryAddPic(String path) {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        Uri contentUri = FileProvider.getUriForFile(this,
                "ru.androiddevschool.fileprovider",
                new File(path));
        mediaScanIntent.setData(contentUri);
        this.sendBroadcast(mediaScanIntent);
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);

        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }

    private File saveBitmap(Bitmap bitmap) {
        File file = null;
        if (bitmap != null) {
            try {
                FileOutputStream outputStream = null;
                try {
                    file = createImageFile();
                    outputStream = new FileOutputStream(file);
                    bitmap.compress(Bitmap.CompressFormat.PNG, 100, outputStream);
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    try {
                        if (outputStream != null) {
                            outputStream.close();
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return file;
    }
}

