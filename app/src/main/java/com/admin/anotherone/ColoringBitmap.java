package com.admin.anotherone;


import android.graphics.Bitmap;
import android.graphics.Paint;

import java.util.ArrayList;
import java.util.HashSet;


public class ColoringBitmap {
    private Bitmap image;
    private Bitmap originalImage;
    private int currentColor;
    private int currentRadius;
    private int imWidth;
    private int imHeight;
    private ArrayList<Integer> current;
    private HashSet<ArrayList<Integer>> fields;
    private int nextX;
    private int nextY;
    private boolean ready;
    private HashSet<Integer> worked;

    public ColoringBitmap(Bitmap bitmap) {
        image = bitmap.copy(Bitmap.Config.ARGB_4444, true);
        originalImage = bitmap.copy(Bitmap.Config.ARGB_4444, true);
        currentRadius = 20;

        imWidth = image.getWidth();
        imHeight = image.getHeight();
        ready = false;
    }

    public void startDraw(float x, float y, Paint paint, int radius) {
        if (!isReady()) prepare();
        currentColor = paint.getColor();
        currentRadius = radius;
        pickCurrent((int) x, (int) y);
        pointFill((int) x, (int) y);
    }

    private void pickCurrent(int x, int y) {
        int num = makeNum(x, y);
        for (ArrayList<Integer> field : fields)
            if (field.contains(num)) {
                current = field;
                return;
            }
    }

    public void prepare() {
        System.out.println("START");
        nextX = 1;
        nextY = 1;
        fields = new HashSet<>();
        worked = new HashSet<>();
        while (nextX != -1) {
            makeCurrent(nextX, nextY);
            fields.add(current);
        }
        System.out.println("READY");
        ready = true;
    }

    public void continueDrawing(float x, float y) {
        pointFill((int) x, (int) y);
    }

    private void pointFill(int x, int y) {
        int cl = originalImage.getPixel(x, y);
        int tX, tY;
        for (Integer num : current) {
            tX = makeX(num);
            tY = makeY(num);
            if (distLess(tX, tY, x, y) && (pointConnected(tX, tY, x, y, cl))) {
                if (image.isMutable())
                    image.setPixel(tX, tY, currentColor);
            }
        }
    }

    private boolean pointConnected(int x1, int y1, int x2, int y2, int color) {
        for (float alpha = 0; alpha < 1.0f; alpha += 0.05)
            if (originalImage.getPixel((int) ((1 - alpha) * x1 + alpha * x2), (int) ((1 - alpha) * y1 + alpha * y2)) != color)
                return false;
        return true;
    }

    public Bitmap getCurrentBitmap() {
        return image;
    }

    private void makeCurrent(int x, int y) {
        int sourceColor = originalImage.getPixel(x, y);
        nextX = -1;
        nextY = -1;
        int tX, tY;
        current = new ArrayList<>();
        addToCurrent(x, y);
        for (int k = 0; k < current.size(); k++) {
            tX = makeX(current.get(k));
            tY = makeY(current.get(k));
            for (int i = Math.max(tX - 1, 0); i <= Math.min(tX + 1, imWidth - 1); i++)
                for (int j = Math.max(tY - 1, 0); j <= Math.min(tY + 1, imHeight - 1); j++)
                    if (originalImage.getPixel(i, j) == sourceColor)
                        addToCurrent(i, j);
                    else if (nextX == -1 && !worked.contains(makeNum(i, j))) {
                        nextX = i;
                        nextY = j;
                    }
        }
    }

    public void addToCurrent(int x, int y) {
        int num = makeNum(x, y);
        if (inCurrent(num)) return;
        else {
            current.add(num);
            worked.add(num);
            //System.out.format("ADDED [%d %d] %d : %d %d\n", originalImage.getWidth(), originalImage.getHeight(), currentLen, x, y);
        }
    }

    private boolean inCurrent(int num) {
        return current.contains(num);
    }

    private int makeNum(int x, int y) {
        return y * originalImage.getWidth() + x;
    }

    private int makeX(int num) {
        return num % originalImage.getWidth();
    }

    private int makeY(int num) {
        return num / originalImage.getWidth();
    }

    private boolean distLess(int x1, int y1, int x2, int y2) {
        return dist2(x1, y1, x2, y2) < currentRadius * currentRadius;
    }

    private int dist2(int x1, int y1, int x2, int y2) {
        return ((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2));
    }

    public boolean isReady() {
        return ready;
    }
}