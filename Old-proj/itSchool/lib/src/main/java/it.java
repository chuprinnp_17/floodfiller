import java.io.File;
import java.io.PrintWriter;
import java.util.Scanner;

/**
 * Created by Гриша on 29.11.2016.
 */
public class it {
    public static void main(String[] args) {
        Scanner sc = null;
        try {
            sc = new Scanner(new File("makesaw.in"));
        } catch (Exception e) {
            e.printStackTrace();
        }
        int t = sc.nextInt();
        int n = sc.nextInt();
        int answer = 0;
        int avrgOdd = 0, avrgEven = 0, minSumDifference, tmpSumDifference;
        if(t == 1) {
            int[] a = new int[n];
            for (int i = 0; i < n; i++) {
                a[i] = sc.nextInt();
                avrgOdd += (i % 2 == 1 ? a[i] : 0);
                avrgEven += (i % 2 == 0 ? a[i] : 0);
            }
            avrgOdd /= n / 2;
            avrgEven /= n / 2;

            minSumDifference = -1;
            for (int i = avrgEven - 1; i <= avrgEven + 1; i++) {
                tmpSumDifference = 0;
                for (int j = 0; j < n; j += 2) tmpSumDifference += Math.abs(i - a[j]);
                if (minSumDifference == -1 || (minSumDifference != -1 && minSumDifference > tmpSumDifference))
                    minSumDifference = tmpSumDifference;
            }
            answer += minSumDifference;

            minSumDifference = -1;
            for (int i = avrgOdd - 1; i <= avrgOdd + 1; i++) {
                tmpSumDifference = 0;
                for (int j = 1; j < n; j += 2) tmpSumDifference += Math.abs(i - a[j]);
                if (minSumDifference == -1 || (minSumDifference != -1 && minSumDifference > tmpSumDifference))
                    minSumDifference = tmpSumDifference;
            }
            answer += minSumDifference;
            try {
                PrintWriter wr = new PrintWriter("makesaw.out");
                wr.print(answer);
                wr.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }else{
            int a1 = sc.nextInt();
            int x = sc.nextInt();
            int y = sc.nextInt();
            int z = sc.nextInt();

            int anext, aprev = a1;
            avrgOdd = a1;
            for (int i = 2; i <= n; i++) {
                anext = (x*aprev+y)%z;
                avrgOdd += (i % 2 == 1 ? anext : 0);
                avrgEven += (i % 2 == 0 ? anext : 0);
            }
            avrgOdd /= n / 2;
            avrgEven /= n / 2;

            и т.д. будет чуть сложнее, но ты справишься

        }

    }
}
