package com.Archieve.Hufman;

/**
 * Created by Гриша on 12.02.2017.
 */
public class Counter<T> {
    public T value;
    public int count;
    public long code;

    public Counter(T value) {
        this.value = value;
        this.count = 0;
        this.code = 0;
    }

    public void add() {
        count++;
    }
    public String toString(){
        return String.format("[%c] -> %d", value, count);
    }
}