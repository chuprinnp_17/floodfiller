package com.Archieve.Hufman;

/**
 * Created by Гриша on 12.02.2017.
 */
public class Node {
    Counter<Character> symbol;
    public Node right;
    public Node left;
    public int weight;
    private boolean type; //true - узел, false - лист

    public Node(Node right, Node left) {
        this.symbol = null;
        this.right = right;
        this.left = left;
        this.type = false;
        this.weight = 0;
        if (right != null) weight += right.weight;
        if (left != null) weight += left.weight;
    }

    public Node(Counter<Character> symbol) {
        this.symbol = symbol;
        this.right = null;
        this.left = null;
        this.type = true;
        this.weight = symbol.count;
    }

    public void print(int spaces) {
        if (right != null) right.print(spaces + 1);
        else {
            for (int i = 0; i < spaces + 1; i++) System.out.print("\t\t");
            System.out.println("____________");
        }
        for (int i = 0; i < spaces + 1; i++) System.out.print("\t\t");
        if (type) {
            for (int i = 0; i < spaces; i++) System.out.print("\t\t");
            System.out.println(symbol);
        }else {
            System.out.format(".%d\n", weight);
        }
        if (left != null) left.print(spaces + 1);
        else {
            for (int i = 0; i < spaces + 1; i++) System.out.print("\t\t");
            System.out.println("____________");
        }
    }
    public void setCode(long code){
        if (type){
            symbol.code = code;
        }else{
            if (right != null) right.setCode(code << 1);
            if (left != null) right.setCode((code << 1) | 1);
        }
    }

    public String toString() {
        if (symbol == null)
            return String.format("%c %c", right == null ? '0' : '1', left == null ? '1' : '0');
        else
            return String.format("%s", symbol);
    }
}
