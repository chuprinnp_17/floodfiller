package com.example;

import java.io.File;
import java.io.FileWriter;
import java.util.Scanner;
public class MyClass {
    public static String str;
    public static void main(String[] args){
        Scanner sc = null;
        try{
            sc = new Scanner(new File("substring.in"));
        }catch(Exception e){
            e.printStackTrace();
        }
        sc.nextInt();
        sc.nextLine();
        str = sc.nextLine();
        int aCount = 0;
        for (char ch: str.toCharArray())
            if (ch == 'a')
                aCount++;
        try{
            FileWriter wr = new FileWriter(new File("substring.out"));
            wr.write(findMaxSub(str, aCount).toString());
            wr.close();
        } catch (Exception e){
            e.printStackTrace();
        }
    }
    public static pair findMaxSub(String data, int aCount){
        if (aCount <= 3)
            return new pair(str.indexOf(data), str.indexOf(data) + data.length());
        int[] n = new int[aCount + 1];
        String[] st = data.split("a");
        for (int i = 0; i < aCount + 1; i++)
            if (st.length > i)
                n[i] = st[i].length();
            else
                n[i] = 0;
        int r = 0, m, s = 0;
        for (int i = 0; i < aCount - 1; i++){
            m = 0;
            for (int j = i; j < i + 3; j++)
                m += n[j];
            if (m > s){
                s = m;
                r = i;
            }
        }
        int f = ((r == 1)?0:find(r)), l = find(((n[0] != 0 && f == 0)?(r + 4):(r + 3)));
        return new pair(f, l);
    }
    public static int find(int r){
        int count = 0;
        if (r == 0)
            return 0;
        for (int i = 0; i < str.length(); i++){
            if (str.charAt(i) == 'a')
                count++;
            if (count == r)
                return i;
        }
        return str.length();
    }
    private static class pair{
        public pair(int a, int b){
            this.a = a;
            this.b = b;
        }
        public int a, b;
        public int length(){
            return b - a;
        }
        public String toString(){
            return (a + 1) + " " + b;
        }
    }
}