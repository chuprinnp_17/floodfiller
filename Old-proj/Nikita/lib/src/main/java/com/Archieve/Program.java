package com.Archieve;

import com.Archieve.Hufman.Counter;
import com.Archieve.Hufman.Node;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

/**
 * Created by Гриша on 12.02.2017.
 */
public class Program {
    public static void main(String[] args) throws IOException {
        HashMap<Character, Counter<Character>> counter = new HashMap<>();
        String path = "D:/1.txt";
        String str;
        Scanner sc = new Scanner(new File(path));
        while(sc.hasNext()){
            str = sc.nextLine();
            for(char symbol : str.toCharArray()) {
                if (!counter.containsKey(symbol)) counter.put(symbol, new Counter<>(symbol));
                counter.get(symbol).add();
            }
        }
        sc.close();
        for(Character chr : counter.keySet()) {
            System.out.format("%s\t", counter.get(chr));
        }
        System.out.println("\n\nСортировочка");

        ArrayList<Node> nodes = new ArrayList<>();
        for(Character chr : counter.keySet()) {
            nodes.add(new Node(counter.get(chr)));
        }
        nodes.sort((o1, o2) -> o2.weight - o1.weight);

        for(Node n : nodes){
            System.out.println(n);
        }

        System.out.println("\n\nДелаем код Хаффмана");
        while(nodes.size()>1){
            Node n1 = nodes.remove(nodes.size()-1);
            Node n2 = nodes.remove(nodes.size()-1);
            nodes.add(new Node(n1, n2));
            nodes.sort((o1, o2) -> o2.weight - o1.weight);
        }

        nodes.get(0).print(0);


    }
}
